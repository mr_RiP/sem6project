﻿using ElderLists.Models;
using ElderListsDatabase;
using ElderListsDatabase.Models.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace ElderLists.Controllers
{
    public class ModerationController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        [Authorize]
        public async Task<ActionResult> Accept(int id)
        {
            var issue = await db.Issues.FindAsync(id);
            if (issue == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            issue.IsAccepted = true;
            await db.SaveChangesAsync();
            return Redirect(Request.UrlReferrer.ToString());
        }

        [Authorize(Roles = "Moderator")]
        public async Task<ActionResult> Resolve(int id)
        {
            var issue = await db.Issues.FindAsync(id);
            if (issue == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (issue.UserId != User.Identity.GetUserId<int>())
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            issue.IsResolved = true;
            await db.SaveChangesAsync();
            return Redirect(Request.UrlReferrer.ToString());
        }

        [Authorize(Roles = "Moderator,Admin")]
        public async Task<ActionResult> Close(int id)
        {
            var issue = await db.Issues.FindAsync(id);
            if (issue == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (issue.UserId == User.Identity.GetUserId<int>() || User.IsInRole("Admin"))
            {
                issue.IsAccepted = true;
                issue.IsResolved = true;
                await db.SaveChangesAsync();
                return Redirect(Request.UrlReferrer.ToString());
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Charge(int id)
        {
            var user = db.Users.Find(id);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int roleId = await db.Roles.Where(r => r.Name == "Moderator").Select(r => r.Id).SingleAsync();
            if (user.Roles.Where(ur => ur.RoleId == roleId).Any())
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            db.UserRoles.Add(new UserRole { RoleId = roleId, UserId = id });
            await db.SaveChangesAsync();
            return Redirect(Request.UrlReferrer.ToString());
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Discharge(int id)
        {
            var user = db.Users.Find(id);
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            int roleId = await db.Roles.Where(r => r.Name == "Moderator").Select(r => r.Id).SingleAsync();
            var userRole = user.Roles.Where(ur => ur.RoleId == roleId).SingleOrDefault();
            if (userRole == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (db.Issues.Where(i => !(i.IsAccepted && i.IsResolved) && i.UserId == id).Any())
            {
                var otherModerators = db.Users
                    .Where(u => u.Roles.Select(ur => ur.RoleId).Contains(roleId))
                    .Except(db.Users.Where(u => u.Id == id));
                if (otherModerators.Any())
                    ViewBag.UserId = new SelectList(otherModerators, "Id", "UserName");

                return View(new DischargeViewModel
                {
                    OriginalUserId = id,
                    OriginalUser = user.UserName,
                    ReturnUrl = Request.UrlReferrer.ToString(),
                });
            }
            else
            {
                db.UserRoles.Remove(userRole);
                await db.SaveChangesAsync();
                return Redirect(Request.UrlReferrer.ToString());
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DischargeRedirect(DischargeViewModel model)
        {
            var issues = await db.Issues
                    .Where(i => !(i.IsAccepted && i.IsResolved) && i.UserId == model.OriginalUserId)
                    .ToListAsync();

            foreach (var item in issues)
                item.UserId = model.UserId;
            await db.SaveChangesAsync();

            await RemoveModerator(model.OriginalUserId);
            return Redirect(model.ReturnUrl);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> DischargeClose(int id, string returnUrl)
        {
            var issues = await db.Issues
                    .Where(i => !(i.IsAccepted && i.IsResolved) && i.UserId == id)
                    .ToListAsync();

            foreach (var item in issues)
            {
                item.IsAccepted = true;
                item.IsResolved = true;
            }
            await db.SaveChangesAsync();

            await RemoveModerator(id);
            return Redirect(returnUrl);
        }

        private async Task RemoveModerator(int userId)
        {
            int roleId = await db.Roles
                .Where(r => r.Name == "Moderator")
                .Select(r => r.Id)
                .SingleAsync();
            var userRole = await db.UserRoles
                .Where(ur => ur.RoleId == roleId && ur.UserId == userId)
                .SingleAsync();
            db.UserRoles.Remove(userRole);
            await db.SaveChangesAsync();
        }
    }
}