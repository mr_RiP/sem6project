﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElderListsDatabase;
using ElderListsDatabase.Models.Identity;
using ElderLists.Models;
using Microsoft.AspNet.Identity;
using ElderLists.Extensions;

namespace ElderLists.Controllers
{
    public class UsersController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        public async Task<ActionResult> Index()
        {
            IQueryable<User> users = db.Users;
            if (!User.IsInRole("Admin"))
            {
                var userRoleId = await db.Roles.Where(r => r.Name == "User").Select(r => r.Id).SingleAsync();
                users = users.Where(u => u.Roles.Select(ur => ur.RoleId).Contains(userRoleId));
            }

            var list = await users
                .Include(u => u.Characters)
                .OrderByDescending(u => u.LastAction)
                .Select(u => new UserListItemViewModel()
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    LastAction = u.LastAction,
                    Characters = u.Characters.Count,
                    HiddenCharacters = u.Characters.Where(c => !c.IsPublic).Count(),
                })
                .ToListAsync();

            return View(list);
        }

        
        public async Task<ActionResult> Moderators()
        {
            var moderatorId = await db.Roles
                .Where(r => r.Name == "Moderator")
                .Select(r => r.Id)
                .SingleAsync();

            var userRoles = await db.UserRoles
                .Where(ur => ur.RoleId == moderatorId)
                .Select(ur => ur.UserId)
                .Distinct()
                .ToListAsync();

            var users = await db.Users
                .Join(userRoles, u => u.Id, ur => ur, (u, ur) => new UserModeratorListItemViewModel
                {
                    Id = u.Id,
                    UserName = u.UserName,
                    Issues = u.Issues.Count,
                    OpenIssues = u.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).Count(),
                    LastAction = u.LastAction,
                })
                .OrderByDescending(vm => vm.LastAction)
                .ToListAsync();

            return View(users);
        }

        [Authorize]
        public ActionResult My()
        {
            return RedirectToAction("Details", new { id = User.Identity.GetUserId<int>() });
        }

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = await db.Users.Where(u => u.Id == id).SingleAsync();
            if (user == null)
                return HttpNotFound();

            var userRolesIds = user.Roles.Select(ur => ur.RoleId).ToList();

            var model = new UserDetailsViewModel
            {
                Id = user.Id,
                UserName = user.UserName,
                About = user.About,
                LastAction = user.LastAction,
                RegisterDate = user.RegisterDate,
                Email = user.IsEmailHidden && !User.IsInRole("Admin") ? null : user.Email,
                IsEmailHidden = user.IsEmailHidden,

                Roles = await db.Roles
                    .Where(r => userRolesIds.Contains(r.Id))
                    .OrderBy(r => r.ViewName)
                    .Select(r => r.ViewName)
                    .ToListAsync(),
                IsModerator = await db.IsUserInRole(user.Id, "Moderator"),
                IsUser = await db.IsUserInRole(user.Id, "User"),

                Characters = await UserCharactersListAsync(user.Id),
                Contacts = new UserContactsViewModel(user),
                Issues = await UserIssuesListAsync(user.Id),
            };

            return View(model);
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Charge(int id)
        {
            var user = await db.Users.Where(u => u.Id == id).SingleOrDefaultAsync();
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var roleId = await db.Roles.Where(r => r.Name == "User").Select(r => r.Id).SingleAsync();

            if (user.Roles.Select(ur => ur.RoleId).Contains(roleId))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            db.UserRoles.Add(new UserRole { UserId = id, RoleId = roleId });
            await db.SaveChangesAsync();

            return Redirect(Request.UrlReferrer.ToString());
        }

        [Authorize(Roles = "Admin")]
        public async Task<ActionResult> Discharge(int id)
        {
            var user = await db.Users.Where(u => u.Id == id).SingleOrDefaultAsync();
            if (user == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var roleId = await db.Roles.Where(r => r.Name == "User").Select(r => r.Id).SingleAsync();
            var userRole = user.Roles.Where(ur => ur.RoleId == roleId).SingleOrDefault();

            if (userRole == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            db.UserRoles.Remove(userRole);
            await db.Characters.Where(c => c.UserId == id).ForEachAsync(c => c.IsPublic = false);
            await db.SaveChangesAsync();

            return Redirect(Request.UrlReferrer.ToString());
        }

        private async Task<List<CharacterListItemViewModel>> UserCharactersListAsync(int userId)
        {
            bool userAdmin = User.IsInRole("Admin");
            bool userModerator = User.IsInRole("Moderator");

            var characters = await db.GetAccessableCharacters(userAdmin, userModerator, userId)
                .OrderByDescending(c => c.LastChanged)
                .Include(c => c.Race)
                
                .Select(c => new CharacterListItemViewModel
                {
                    Id = c.Id,
                    AuthorId = c.Id,
                    Name = c.Name,
                    IsPublic = c.IsPublic,
                    LastChanged = c.LastChanged,
                    Class = c.Class.Name,
                    Race = c.Race.SpecialName == null ? 
                        c.Race.CommonName : (c.Race.CommonName.Length < c.Race.SpecialName.Length ?
                            c.Race.CommonName : c.Race.SpecialName),
                    HaveIssues = c.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).Any(),
                }).ToListAsync();


            return characters.Count == 0 ? null : characters;
        }

        private async Task<List<ModerationIssueListItemViewModel>> UserIssuesListAsync(int userId)
        {
            if (User.IsInRole("Moderator") || User.IsInRole("Admin"))
            {
                var issues = await db.Issues
                    .Where(i => i.UserId == userId)
                    .Include(i => i.Characters).Include(i => i.User)
                    .OrderByDescending(i => i.LastReview)
                    .ToListAsync();

                if (issues.Count != 0)
                    return issues
                        .Select(i => new ModerationIssueListItemViewModel(i))
                        .ToList();
            }
            return null;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
