﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ElderListsDatabase;
using ElderListsDatabase.Models;
using ElderLists.Models;
using Microsoft.AspNet.Identity;
using ElderLists.Extensions;

namespace ElderLists.Controllers
{
    public class CharactersController : Controller
    {
        private DatabaseContext db = new DatabaseContext();

        // GET: Characters
        public async Task<ActionResult> Index()
        {
            var characters = await db.GetAccessableCharacters(User.IsInRole("Admin"), User.IsInRole("Moderator"))
                .OrderByDescending(c => c.LastChanged)
                .Include(c => c.Race)
                .Select(c => new CharacterListItemViewModel
                {
                    Id = c.Id,
                    AuthorId = c.UserId,
                    Author = c.User.UserName,
                    Name = c.Name,
                    IsPublic = c.IsPublic,
                    LastChanged = c.LastChanged,
                    Class = c.Class.Name,
                    Race = c.Race.SpecialName == null ?
                        c.Race.CommonName : (c.Race.CommonName.Length < c.Race.SpecialName.Length ?
                            c.Race.CommonName : c.Race.SpecialName),
                    HaveIssues = c.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).Any(),
                }).ToListAsync();

            return View(characters);
        }

        // GET: Characters/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Character character = await db.Characters.FindAsync(id);
            if (character == null)
                return HttpNotFound();

            bool loadIssue = User.Identity.GetUserId<int>() == character.UserId || User.IsInRole("Moderator") || User.IsInRole("Admin");
            return View(new CharacterDetailsViewModel(character, loadIssue));
        }

        [Authorize]
        public async Task<ActionResult> My()
        {
            int userId = User.Identity.GetUserId<int>();
            var characters = db.Characters
                .Where(c => c.UserId == userId)
                .OrderByDescending(c => c.LastChanged)
                .Include(c => c.Race)
                .Select(c => new CharacterListItemViewModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    Race = c.Race.SpecialName == null ? c.Race.CommonName : (c.Race.CommonName.Length < c.Race.SpecialName.Length ? c.Race.CommonName : c.Race.SpecialName),
                    Class = c.Class.Name,
                    LastChanged = c.LastChanged,
                    IsPublic = c.IsPublic,
                    HaveIssues = c.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).Any(),
                });
            return View(await characters.ToListAsync());
        }

        [Authorize(Roles = "Moderator,Admin")]
        public async Task<ActionResult> Moderation()
        {
            if (!(await db.IsUserInRole(User.Identity.GetUserId<int>(), "Moderator") || await db.IsUserInRole(User.Identity.GetUserId<int>(), "Admin")))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            var characters = db.Characters
                .Where(c => c.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).Any())
                .OrderByDescending(c => c.LastChanged)
                .Select(c => new
                {
                    Character = c,
                    Issue = c.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).FirstOrDefault()
                })
                .Select(ci => new ModerationCharacterListItemViewModel
                {
                    Id = ci.Character.Id,
                    Name = ci.Character.Name,
                    LastChanged = ci.Character.LastChanged,
                    AuthorId = ci.Character.UserId,
                    Author = ci.Character.User.UserName,
                    ModeratorId = ci.Issue.UserId,
                    Moderator = ci.Issue.User.UserName,
                    IsAccepted = ci.Issue.IsAccepted,
                    IsResolved = ci.Issue.IsResolved,
                    LastReview = ci.Issue.LastReview,
                });
            return View(await characters.ToListAsync());
        }

        // GET: Characters/Create
        [Authorize(Roles = "User")]
        public async Task<ActionResult> Create()
        {
            if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "User"))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.AlignmentId = new SelectList(db.Alignments, "Id", "Name");
            ViewBag.ClassId = new SelectList(db.Classes.OrderBy(o => o.Name), "Id", "Name");
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Name");
            ViewBag.RaceId = CreateRacesSelectList();

            return View();
        }

        // POST: Characters/Create
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> Create([Bind(Include = "Name,LastChanged,IsPublic,RaceId,GenderId,AbsoluteAge," +
            "RelativeAge,Appearance,CombatGear,CommonGear,SpecialSigns,AlignmentId,Personality,Vices,Virtues,Occupation," +
            "ClassId,CombatTactics,SkillsSource")] CharacterEditViewModel model)
        {
            if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "User"))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (ModelState.IsValid)
            {
                var character = await AddOrUpdateDatabaseModelAsync(model);
                await UpdateUserLastAction();
                return RedirectToAction("Details", new { id = character.Id });
            }

            ViewBag.AlignmentId = new SelectList(db.Alignments, "Id", "Name", model.AlignmentId);
            ViewBag.ClassId = new SelectList(db.Classes.OrderBy(o => o.Name), "Id", "Name", model.ClassId);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Name", model.GenderId);
            ViewBag.RaceId = CreateRacesSelectList(model.RaceId);

            return View(model);
        }

        // GET: Characters/Edit/5
        [Authorize]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> Edit(int id)
        {
            if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "User"))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            Character character = await db.Characters.FindAsync(id);
            if (character == null)
                return HttpNotFound();

            ViewBag.AlignmentId = new SelectList(db.Alignments, "Id", "Name", character.AlignmentId);
            ViewBag.ClassId = new SelectList(db.Classes.OrderBy(o => o.Name), "Id", "Name", character.ClassId);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Name", character.GenderId);
            ViewBag.RaceId = CreateRacesSelectList(character.RaceId);

            return View(new CharacterEditViewModel(character));
        }

        // POST: Characters/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "User")]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,UserId,IsPublic,RaceId,GenderId,AbsoluteAge," +
            "RelativeAge,Appearance,CombatGear,CommonGear,SpecialSigns,AlignmentId,Personality,Vices,Virtues," +
            "Occupation,ClassId,CombatTactics,SkillsSource")] CharacterEditViewModel model)
        {
            if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "User"))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            if (ModelState.IsValid)
            {

                var character = await AddOrUpdateDatabaseModelAsync(model);
                await UpdateVisibility(character);
                await UpdateUserLastAction();
                return RedirectToAction("Details", new { id = character.Id });
            }

            ViewBag.AlignmentId = new SelectList(db.Alignments, "Id", "Name", model.AlignmentId);
            ViewBag.ClassId = new SelectList(db.Classes.OrderBy(o => o.Name), "Id", "Name", model.ClassId);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Name", model.GenderId);
            ViewBag.RaceId = CreateRacesSelectList(model.RaceId);

            return View(model);
        }

        // GET: Characters/Edit/5
        [Authorize(Roles = "Moderator")]
        public async Task<ActionResult> Fix(int id)
        {
            if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "Moderator"))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            Character character = await db.Characters.FindAsync(id);
            if (character == null)
                return HttpNotFound();

            if (User.Identity.GetUserId<int>() == character.UserId)
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            ViewBag.AlignmentId = new SelectList(db.Alignments, "Id", "Name", character.AlignmentId);
            ViewBag.ClassId = new SelectList(db.Classes.OrderBy(o => o.Name), "Id", "Name", character.ClassId);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Name", character.GenderId);
            ViewBag.RaceId = CreateRacesSelectList(character.RaceId);

            return View(new CharacterFixViewModel(character));
        }

        // POST: Characters/Edit/5
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Moderator")]
        public async Task<ActionResult> Fix([Bind(Include = "Id,Name,UserId,IsPublic,RaceId,GenderId," +
            "AbsoluteAge,RelativeAge,Appearance,CombatGear,CommonGear,SpecialSigns,AlignmentId,Personality,Vices," +
            "Virtues,Occupation,ClassId,CombatTactics,SkillsSource,IssueText,IsResolved,IsPrivateLocked")] CharacterFixViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "Moderator"))
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                var character = await AddOrUpdateDatabaseModelAsync(model);
                await AddOrUpdateIssue(character, model.IssueText, model.IsResolved, model.IsPrivateLocked);
                await UpdateVisibility(character);
                await UpdateUserLastAction();
                return RedirectToAction("Details", new { id = character.Id });
            }

            ViewBag.AlignmentId = new SelectList(db.Alignments, "Id", "Name", model.AlignmentId);
            ViewBag.ClassId = new SelectList(db.Classes.OrderBy(o => o.Name), "Id", "Name", model.ClassId);
            ViewBag.GenderId = new SelectList(db.Genders, "Id", "Name", model.GenderId);
            ViewBag.RaceId = CreateRacesSelectList(model.RaceId);

            return View(model);
        }

        // GET: Characters/Delete/5
        [Authorize]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            Character character = await db.Characters.FindAsync(id);
            if (character == null)
                return HttpNotFound();

            if (await db.IsUserInRole(User.Identity.GetUserId<int>(), "Admin") || User.Identity.GetUserId<int>() == character.UserId)
                return View(new CharacterDeleteViewModel { Name = character.Name });
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

        }

        // POST: Characters/Delete/5
        [Authorize]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Character character = await db.Characters.FindAsync(id);

            if (await db.IsUserInRole(User.Identity.GetUserId<int>(), "Admin") || User.Identity.GetUserId<int>() == character.UserId)
            {
                db.Issues.RemoveRange(character.Issues);
                db.Characters.Remove(character);
                await db.SaveChangesAsync();
                return RedirectToAction("Index", "Characters", "My");
            }
            else
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);
        }

        // [ChildActionOnly] // Вызывается скриптом
        public async Task<ActionResult> ClassDetails(int id)
        {
            return PartialView(await CreateClassViewModelAsync(id));
        }

        // [ChildActionOnly] // Вызывается скриптом
        public async Task<ActionResult> RaceDetails(int id)
        {
            var race = await db.Races.FindAsync(id);
            var links = race.Links.Select(l => new LinkViewModel { ResourceName = l.ResourceName, Url = l.Url }).ToList();
            return PartialView(links);
        }

        // [ChildActionOnly] // Вызывается скриптом
        public async Task<ActionResult> AlignmentDetails(int id)
        {
            var alignment = await db.Alignments.FindAsync(id);
            var links = alignment.Links.Select(l => new LinkViewModel { ResourceName = l.ResourceName, Url = l.Url }).ToList();
            return PartialView(links);
        }

        public async Task<ActionResult> IsRaceLongLiver(int raceId)
        {
            return Json((await db.Races.FindAsync(raceId)).IsLongLiver, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Moderator")]
        public async Task<ActionResult> Review(int id)
        {
            if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "Moderator"))
                return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

            Character character = await db.Characters.FindAsync(id);
            if (character == null)
                return HttpNotFound();

            return View(new CharacterReviewViewModel(character));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Moderator")]
        public async Task<ActionResult> Review([Bind(Include = "Id,IssueText,IsResolved,IsPrivateLocked")] CharacterReviewViewModel model)
        {
            var character = await db.Characters.FindAsync(model.Id);
            if (ModelState.IsValid)
            {
                if (!await db.IsUserInRole(User.Identity.GetUserId<int>(), "Moderator"))
                    return new HttpStatusCodeResult(HttpStatusCode.Forbidden);

                await AddOrUpdateIssue(character, model.IssueText, model.IsResolved, model.IsPrivateLocked);
                await UpdateVisibility(character);
                return RedirectToAction("Details", new { id = character.Id });
            }

            return View(new CharacterReviewViewModel(character));
        }

        public async Task<ActionResult> About()
        {
            var races = await db.Races
                .OrderBy(r => r.CommonName)
                .Select(r => new RaceViewModel
                {
                    Name = r.SpecialName == null ? r.CommonName : r.CommonName + " (" + r.SpecialName + ")",
                    Links = r.Links.Select(l => new LinkViewModel { ResourceName = l.ResourceName, Url = l.Url }).ToList(),
                })
                .ToListAsync();

            var classes = await db.Classes
                .OrderBy(c => c.Name)
                .Select(c => new ClassViewModel
                {
                    Id = c.Id,
                    Name = c.Name,
                    Description = c.Description,
                    TypeName = c.Type.Name,
                })
                .ToListAsync();

            return View(new CharactersAboutViewModel { Races = races, Classes = classes });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Вспомогательные функции
        private SelectList CreateRacesSelectList(int? selectedId = null)
        {
            var races = db.Races
                .Select(r => new RaceListItemViewModel
                {
                    Id = r.Id,
                    Name = r.SpecialName == null ? r.CommonName : r.CommonName + " (" + r.SpecialName + ")",
                })
                .OrderBy(r => r.Name);
            if (selectedId == null)
                return new SelectList(races, "Id", "Name");
            else
                return new SelectList(races, "Id", "Name", selectedId);
        }

        private async Task<Character> AddOrUpdateDatabaseModelAsync(CharacterEditViewModel model)
        {
            var character = await db.Characters.FindAsync(model.Id);
            if (character == null)
            {
                character = new Character();
                character.CreationDate = DateTime.Now;
                character.LastChanged = character.CreationDate;
                character.UserId = User.Identity.GetUserId<int>(); 
                db.Characters.Add(character);
            }
            else
            {
                character.LastChanged = DateTime.Now;
            }

            character.Name = model.Name;
            character.IsPublic = model.IsPublic;

            character.GenderId = model.GenderId;
            character.RaceId = model.RaceId;
            character.AbsoluteAge = model.AbsoluteAge;
            character.RelativeAge = (await db.Races.FindAsync(model.RaceId)).IsLongLiver ? model.RelativeAge : null;
            character.Appearance = model.Appearance;
            character.CombatGear = model.CombatGear;
            character.CommonGear = model.CommonGear;
            character.SpecialSigns = model.SpecialSigns;

            character.AlignmentId = model.AlignmentId;
            character.Personality = model.Personality;
            character.Vices = model.Vices;
            character.Virtues = model.Virtues;

            character.ClassId = model.ClassId;
            character.Occupation = model.Occupation;
            character.CombatTactics = model.CombatTactics;
            character.SkillsSource = model.SkillsSource;

            await db.SaveChangesAsync();
            return character;
        }

        private async Task<ClassViewModel> CreateClassViewModelAsync(int id)
        {
            var dbClass = await db.Classes.FindAsync(id);
            return new ClassViewModel
            {
                Id = dbClass.Id,
                Name = dbClass.Name,
                Description = dbClass.Description,
                TypeName = dbClass.Type.Name
            };
        }

        private async Task AddOrUpdateIssue(Character character, string text, bool isResolved, bool isPrivateLocked)
        {
            var issue = character.ActualIssue();
            var time = DateTime.Now;
            if (issue == null)
            {
                issue = new Issue { CreationDate = time };
                db.Issues.Add(issue);
                character.Issues.Add(issue);
            }

            issue.UserId = User.Identity.GetUserId<int>();
            issue.Text = text;
            issue.IsPrivateLocked = isPrivateLocked;
            issue.IsResolved = isResolved;
            issue.IsAccepted = false;
            issue.LastReview = time;

            await db.SaveChangesAsync();
        }

        private async Task UpdateVisibility(Character character)
        {
            if (character.IsPrivateLocked())
            {
                character.IsPublic = false;
                await db.SaveChangesAsync();
            }
        }

        private async Task UpdateUserLastAction()
        {
            var user = db.Users.Find(User.Identity.GetUserId<int>());
            user.LastAction = DateTime.Now;
            await db.SaveChangesAsync();
        }
        #endregion
    }
}
