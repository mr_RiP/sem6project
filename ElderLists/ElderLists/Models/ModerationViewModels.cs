﻿using ElderListsDatabase.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElderLists.Models
{
    public class IssueViewModel
    {
        public IssueViewModel()
        {

        }

        public IssueViewModel(Issue issue)
        {
            Id = issue.Id;
            Moderator = issue.User.UserName;
            ModeratorId = issue.UserId;
            Text = issue.Text;
            LastReview = issue.LastReview;
            IsAccepted = issue.IsAccepted;
            IsPrivateLocked = issue.IsPrivateLocked;
            IsResolved = issue.IsResolved;
        }

        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "ID Модератора")]
        public int ModeratorId { get; set; }

        [Display(Name = "Модератор")]
        public string Moderator { get; set; }

        [Display(Name = "Замечания")]
        [DataType(DataType.MultilineText)]
        public string Text { get; set; }

        [Display(Name = "Последний просмотр")]
        public DateTime LastReview { get; set; }

        [Display(Name = "Разрешено")]
        public bool IsResolved { get; set; }

        [Display(Name = "Принято")]
        public bool IsAccepted { get; set; }

        [Display(Name = "Заблокирован")]
        public bool IsPrivateLocked { get; set; }
    }

    public class DischargeViewModel
    {
        public int OriginalUserId { get; set; }

        [Display(Name = "Текущий модератор")]
        public string OriginalUser { get; set; }

        [Display(Name = "Новый модератор")]
        public int UserId { get; set; }

        [Display(Name = "Заметки модератора")]
        public int Issues { get; set; }

        public string ReturnUrl { get; set; }
    }

    public class ModerationIssueListItemViewModel
    {
        public ModerationIssueListItemViewModel()
        {

        }

        public ModerationIssueListItemViewModel(Issue issue)
        {
            LastReview = issue.LastReview;
            UserId = issue.UserId;
            User = issue.User.UserName;

            SetupTarget(issue);
            SetupState(issue);
        }

        private void SetupState(Issue issue)
        {
            if (issue.IsAccepted && issue.IsResolved)
                State = "Закрыто";
            else if (issue.IsAccepted)
                State = "Принято";
            else if (issue.IsResolved)
                State = "Разрешено";
            else
                State = "Открыто";
        }

        private void SetupTarget(Issue issue)
        {
            if (issue.Characters.Any())
            {
                var character = issue.Characters.Single();
                Target = character.Name;
                TargetController = "Characters";
                TargetId = character.Id;
            }
        }

        [Display(Name = "Цель")]
        public string Target { get; set; }

        [Display(Name = "ID Цели")]
        public int? TargetId { get; set; }
        public string TargetController { get; set; }

        [Display(Name = "Дата последней проверки")]
        public DateTime LastReview { get; set; }

        [Display(Name = "ID Модератора")]
        public int UserId { get; set; }

        [Display(Name = "Модератор")]
        public string User { get; set; }
        
        [Display(Name = "Состояние")]
        public string State { get; set; }
    }
}