﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

namespace ElderLists.Models
{
    public class IndexViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }
        public bool IsEmailHidden { get; set; }

        [Display(Name = "Примечание")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024)]
        public string About { get; set; }

        [Display(Name = "Роли")]
        public IEnumerable<string> Roles { get; set; }

        public UserContactsViewModel Contacts { get; set; }

        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать символов не менее: {2}.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение нового пароля")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать символов не менее: {2}.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение нового пароля")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Номер телефона")]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = "Код")]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Номер телефона")]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }

    public class ChangeUserNameViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать не менее {2} и не более {1} символов.", MinimumLength = 1)]
        [Display(Name = "Новое имя")]
        public string NewUserName { get; set; }
    }

    public class ChangeEmailViewModel
    {
        [Display(Name = "Текущий E-mail")]
        public string OldEmail { get; set; }

        [EmailAddress]
        [Display(Name = "Новый E-mail")]
        public string NewEmail { get; set; }

        [Display(Name = "Скрыть Email")]
        public bool HideEmail { get; set; }
    }

    public class ChangeAboutViewModel
    {
        [Display(Name = "Примечание")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024, ErrorMessage = "В тексте примечания не должно быть больше {1} символов.")]
        public string About { get; set; }
    }

    public class ChangeUserPasswordViewModel
    {
        [Required]
        [Display(Name = "ID пользователя")]
        public int Id { get; set; }

        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Пароль администратора")]
        public string AdminPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Значение {0} должно содержать символов не менее: {2}.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение нового пароля")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class DeleteUserViewModel
    {
        [Required]
        [Display(Name = "ID пользователя")]
        public int Id { get; set; }

        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Пароль администратора")]
        public string AdminPassword { get; set; }
    }
}