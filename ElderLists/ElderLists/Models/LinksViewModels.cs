﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ElderLists.Models
{
    public class LinkViewModel
    {
        public string ResourceName { get; set; }

        public string Url { get; set; }
    }
}