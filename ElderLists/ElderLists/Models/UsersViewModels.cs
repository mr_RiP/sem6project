﻿using ElderListsDatabase.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ElderLists.Models
{
    public class UserListItemViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string UserName { get; set; }

        [Display(Name = "Персонажи")]
        public int Characters { get; set; }

        [Display(Name = "Скрытые персонажи")]
        public int HiddenCharacters { get; set; }

        [Display(Name = "Дата последнего действия")]
        public DateTime LastAction { get; set; }
    }

    public class UserModeratorListItemViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string UserName { get; set; }

        [Display(Name = "Замечания")]
        public int Issues { get; set; }

        [Display(Name = "Активные замечания")]
        public int OpenIssues { get; set; }

        [Display(Name = "Дата последнего действия")]
        public DateTime LastAction { get; set; }
    }

    public class UserDetailsViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Display(Name = "E-mail")]
        public string Email { get; set; }

        [Display(Name = "Примечание")]
        [DataType(DataType.MultilineText)]
        [StringLength(1024)]
        public string About { get; set; }

        [Display(Name = "Дата регистрации")]
        public DateTime RegisterDate { get; set; }

        [Display(Name = "Дата последнего действия")]
        public DateTime LastAction { get; set; }

        [Display(Name = "Роли")]
        public IEnumerable<string> Roles { get; set; }

        [Display(Name = "В роли модератора")]
        public bool IsModerator { get; set; }

        [Display(Name = "В роли пользователя")]
        public bool IsUser { get; set; }

        [Display(Name = "E-mail скрыт")]
        public bool IsEmailHidden { get; set; }

        public IEnumerable<CharacterListItemViewModel> Characters { get; set; }

        public UserContactsViewModel Contacts { get; set; }

        public IEnumerable<ModerationIssueListItemViewModel> Issues { get; set; }
    }

    public class UserContactsViewModel
    {
        public UserContactsViewModel()
        {
        }

        public UserContactsViewModel(User user)
        {
            Discord = user.Discord;
            EsoUserId = user.EsoUserId;
            Skype = user.Skype;
            Telegram = user.Telegram;
        }

        [StringLength(32)]
        [Display(Name = "Telegram")]
        public string Telegram { get; set; }

        [StringLength(32)]
        [Display(Name = "Discord")]
        public string Discord { get; set; }

        [StringLength(32)]
        [Display(Name = "Skype")]
        public string Skype { get; set; }

        [StringLength(32)]
        [Display(Name = "ESO UserID")]
        public string EsoUserId { get; set; }
    }
}