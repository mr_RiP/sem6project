﻿using ElderLists.Extensions;
using ElderListsDatabase;
using ElderListsDatabase.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ElderLists.Models
{
    public class CharacterListItemViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Раса")]
        public string Race { get; set; }

        [Display(Name = "Класс")]
        public string Class { get; set; }

        [Display(Name = "ID Автора")]
        public int AuthorId { get; set; }

        [Display(Name = "Автор")]
        public string Author { get; set; }

        [Display(Name = "Последнее изменение")]
        public DateTime LastChanged { get; set; }

        [Display(Name = "Открытый доступ")]
        public bool IsPublic { get; set; }

        [Display(Name = "Активные замечания")]
        public bool HaveIssues { get; set; }
    }

    public class ModerationCharacterListItemViewModel : CharacterListItemViewModel
    {
        [Display(Name = "ID Модератора")]
        public int ModeratorId { get; set; }

        [Display(Name = "Модератор")]
        public string Moderator { get; set; }

        [Display(Name = "Последняя проверка")]
        public DateTime LastReview { get; set; }

        public bool IsAccepted { get; set; }
        public bool IsResolved { get; set; }

        [Display(Name = "Состояние")]
        public string State
        {
            get
            {
                if (IsAccepted && IsResolved)
                    return "Закрыто";
                else if (IsAccepted)
                    return "Ожидает проверки";
                else if (IsResolved)
                    return "Разрешено";
                else
                    return "Открыто";
            }
        }
    }

    public class CharacterDetailsViewModel
    {
        public CharacterDetailsViewModel()
        {
        }

        public CharacterDetailsViewModel(Character character, bool loadIssue = true)
        {

            Id = character.Id;
            Name = character.Name;

            UserId = character.UserId;
            UserName = character.User.UserName;
            LastChanged = character.LastChanged;
            CreationDate = character.CreationDate;

            Gender = character.Gender.Name;
            Race = character.Race.FullName();
            AbsoluteAge = character.AbsoluteAge;
            RelativeAge = character.RelativeAge;
            Appearance = character.Appearance;
            CombatGear = character.CombatGear;
            CommonGear = character.CommonGear;
            SpecialSigns = character.SpecialSigns;

            Alignment = character.Alignment.Name;
            Personality = character.Personality;
            Vices = character.Vices;
            Virtues = character.Virtues;

            Class = character.Class.Name;
            Occupation = character.Occupation;
            CombatTactics = character.CombatTactics;
            SkillsSource = character.SkillsSource;

            IsPublic = character.IsPublic;

            if (loadIssue)
            {
                var issue = character.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).SingleOrDefault();
                if (issue != null)
                    Issue = new IssueViewModel(issue);
            }
        }

        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Открытый доступ")]
        public bool IsPublic { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Раса")]
        public string Race { get; set; }

        [Display(Name = "Пол")]
        public string Gender { get; set; }

        [Display(Name = "Возраст")]
        public int? AbsoluteAge { get; set; }

        [Display(Name = "Относительный возраст")]
        public int? RelativeAge { get; set; }

        [Display(Name = "Общее описание внешности")]
        [DataType(DataType.MultilineText)]
        public string Appearance { get; set; }

        [Display(Name = "Боевое снаряжение")]
        [DataType(DataType.MultilineText)]
        public string CombatGear { get; set; }

        [Display(Name = "Гражданская одежда")]
        [DataType(DataType.MultilineText)]
        public string CommonGear { get; set; }

        [Display(Name = "Особые приметы")]
        [DataType(DataType.MultilineText)]
        public string SpecialSigns { get; set; }

        [Display(Name = "Мировоззрение")]
        public string Alignment { get; set; }

        [Display(Name = "Общее описание характера")]
        [DataType(DataType.MultilineText)]
        public string Personality { get; set; }

        [Display(Name = "Отрицательные черты")]
        [DataType(DataType.MultilineText)]
        public string Vices { get; set; }

        [Display(Name = "Положительные черты")]
        [DataType(DataType.MultilineText)]
        public string Virtues { get; set; }

        [Display(Name = "Класс")]
        public string Class { get; set; }

        [Display(Name = "Род занятий")]
        [DataType(DataType.MultilineText)]
        public string Occupation { get; set; }

        [Display(Name = "Тактика боя")]
        [DataType(DataType.MultilineText)]
        public string CombatTactics { get; set; }

        [Display(Name = "Источники умений")]
        [DataType(DataType.MultilineText)]
        public string SkillsSource { get; set; }

        [Display(Name = "ID Автора")]
        public int UserId { get; set; }

        [Display(Name = "Автор")]
        public string UserName { get; set; }

        [Display(Name = "Дата создания")]
        public DateTime CreationDate { get; set; }

        [Display(Name = "Последнее изменение")]
        public DateTime LastChanged { get; set; }

        public IssueViewModel Issue { get; set; }

    }

    public class ClassViewModel
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Тип")]
        public string TypeName { get; set; }
    }

    public class CharacterDeleteViewModel
    {
        [Display(Name = "Имя")]
        public string Name { get; set; }
    }

    public class RaceListItemViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class CharacterEditViewModel
    {
        public CharacterEditViewModel()
        {
        }

        public CharacterEditViewModel(Character character, bool loadIssue = true)
        {
            Id = character.Id;
            Name = character.Name;

            IsPublic = character.IsPublic;
            UserId = character.UserId;

            GenderId = character.GenderId;
            RaceId = character.RaceId;
            AbsoluteAge = character.AbsoluteAge;
            RelativeAge = character.RelativeAge;
            Appearance = character.Appearance;
            CombatGear = character.CombatGear;
            CommonGear = character.CommonGear;
            SpecialSigns = character.SpecialSigns;

            AlignmentId = character.AlignmentId;
            Personality = character.Personality;
            Vices = character.Vices;
            Virtues = character.Virtues;

            ClassId = character.ClassId;
            Occupation = character.Occupation;
            CombatTactics = character.CombatTactics;
            SkillsSource = character.SkillsSource;

            if (loadIssue)
            {
                var issue = character.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).SingleOrDefault();
                if (issue != null)
                    Issue = new IssueViewModel(issue);
            }
        }

        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "ID Автора")]
        public int UserId { get; set; }

        [Required]
        [Display(Name = "Открытый доступ",
            Description = "Разрешить отображение материала в публичных разделах сайта.")]
        public bool IsPublic { get; set; }

        [Required]
        [StringLength(128, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Имя",
            Description = "Имя персонажа.")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Раса",
            Description = "Раса, к которой принадлежит персонаж.")]
        public int RaceId { get; set; }

        [Required]
        [Display(Name = "Пол",
            Description = "Биологический пол персонажа.")]
        public int GenderId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Возраст персонажа не может быть равным нулю или отрицательным.")]
        [Display(Name = "Возраст",
            Description = "Хронологический возраст персонажа.")]
        public int? AbsoluteAge { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Относительный возраст персонажа не может быть равным нулю или отрицательным.")]
        [Display(Name = "Относительный возраст",
            Description = "Возраст персонажа в человеческих годах.")]
        public int? RelativeAge { get; set; }

        [Required]
        [StringLength(1024, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Общее описание внешности",
            Description = "Общее описание внешних черт персонажа.")]
        [DataType(DataType.MultilineText)]
        public string Appearance { get; set; }

        [StringLength(512, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Боевое снаряжение",
            Description = "Внешнее описание обычно используемого персонажем боевого снаряжаения.")]
        [DataType(DataType.MultilineText)]
        public string CombatGear { get; set; }

        [StringLength(512, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Гражданская одежда",
            Description = "Внешнее описание предпочтений персонажа в небоевой экипировке.")]
        [DataType(DataType.MultilineText)]
        public string CommonGear { get; set; }

        [StringLength(512, ErrorMessage = "Максимальное число символов для поля {0}: {1}")]
        [Display(Name = "Особые приметы",
            Description = "Особые приметы, по которым может быть опознан персонаж. Например, заметные шрамы, татуировки или странная манера речи.")]
        [DataType(DataType.MultilineText)]
        public string SpecialSigns { get; set; }

        [Required]
        [Display(Name = "Мировоззрение",
            Description = "Мировоззрение персонажа согласно системе D&D 3.5.")]
        public int AlignmentId { get; set; }

        [Required]
        [StringLength(1024, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Общее описание характера",
            Description = "Общее описание личностных качеств персонажа.")]
        [DataType(DataType.MultilineText)]
        public string Personality { get; set; }

        [StringLength(512, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Отрицательные черты",
            Description = "Черты характера, которые создают персонажу проблемы.")]
        [DataType(DataType.MultilineText)]
        public string Vices { get; set; }

        [StringLength(512, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Положительные черты",
            Description = "Черты характера, которые помогают персонажу.")]
        [DataType(DataType.MultilineText)]
        public string Virtues { get; set; }

        [Required]
        [Display(Name = "Класс",
            Description = "Класс отражает общую направленность навыков персонажа.")]
        public int ClassId { get; set; }

        [Required]
        [StringLength(1024, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Род занятий",
            Description = "Деятельность, обеспечивающая персонажа средствами к существованию, его место в мире.")]
        [DataType(DataType.MultilineText)]
        public string Occupation { get; set; }

        [StringLength(512, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Тактика боя",
            Description = "Описание предпочитаемой персонажем тактики ведения боя.")]
        [DataType(DataType.MultilineText)]
        public string CombatTactics { get; set; }

        [StringLength(512, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [Display(Name = "Источники умений",
            Description = "Учителя, организации или события, в результате которых персонаж получил свои навыки.")]
        [DataType(DataType.MultilineText)]
        public string SkillsSource { get; set; }


        public IssueViewModel Issue { get; set; }
    }

    public class CharacterFixViewModel : CharacterEditViewModel
    {
        public CharacterFixViewModel()
        {

        }

        public CharacterFixViewModel(Character character) : base(character, false)
        {
            var issue = character.ActualIssue();
            if (issue != null)
            {
                IssueText = issue.Text;
                IsResolved = issue.IsResolved;
                IsPrivateLocked = issue.IsPrivateLocked;
            }
        }

        [Required]
        [Display(Name = "Текст замечания",
            Description = "Укажите изменения, которые вы внесли в материал, и их причны.")]
        [StringLength(1024, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [DataType(DataType.MultilineText)]
        public string IssueText { get; set; }

        [Required]
        [Display(Name = "Ограничение доступа",
            Description = "Запретить автору публиковать материал.")]
        public bool IsPrivateLocked { get; set; }

        [Required]
        [Display(Name = "Проблема решена",
            Description = "Подтверждение автора аннулирует это замечание.")]
        public bool IsResolved { get; set; }
    }

    public class CharacterReviewViewModel : CharacterDetailsViewModel
    {
        public CharacterReviewViewModel()
        {

        }

        public CharacterReviewViewModel(Character character) : base(character, false)
        {
            var issue = character.ActualIssue();
            if (issue != null)
            {
                IssueText = issue.Text;
                IsResolved = issue.IsResolved;
                IsPrivateLocked = issue.IsPrivateLocked;
            }
        }

        [Required]
        [Display(Name = "Текст замечания",
            Description = "Укажите изменения, которые вы бы хотели увидеть в материале, и их причны.")]
        [StringLength(1024, ErrorMessage = "Максимальное число символов для поля {0}: {1}.")]
        [DataType(DataType.MultilineText)]
        public string IssueText { get; set; }

        [Required]
        [Display(Name = "Проблема решена",
            Description = "Подтверждение автора аннулирует это замечание.")]
        public bool IsResolved { get; set; }

        [Required]
        [Display(Name = "Ограничение доступа",
            Description = "Запретить автору публиковать материал.")]
        public bool IsPrivateLocked { get; set; }
    }

    public class CharactersAboutViewModel
    {
        [Display(Name = "Классы")]
        public IEnumerable<ClassViewModel> Classes { get; set; }

        [Display(Name = "Расы")]
        public IEnumerable<RaceViewModel> Races { get; set; }
    }

    public class RaceViewModel
    {
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Ссылки")]
        public IEnumerable<LinkViewModel> Links { get; set; }
    }
}