﻿using ElderListsDatabase;
using ElderListsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ElderLists.Extensions
{
    public static class RaceExtesions
    {
        public static string ShortName(this Race race)
        {
            if (race.SpecialName == null)
                return race.CommonName;
            else
                return race.CommonName.Length < race.SpecialName.Length ? race.CommonName : race.SpecialName;
        }

        public static string Name(this Race race)
        {
            return race.SpecialName ?? race.CommonName;
        }

        public static string FullName(this Race race)
        {
            return race.SpecialName == null ? race.CommonName : race.CommonName + " (" + race.SpecialName + ")";
        }
    }

    public static class CharacterExtensions
    {
        public static Issue ActualIssue(this Character character)
        {
            return character.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).SingleOrDefault();
        }

        public static bool IsPrivateLocked(this Character character)
        {
            var issue = character.ActualIssue();
            return issue != null && issue.IsPrivateLocked;
        }
    }

    public static class DatabaseContextExtensions
    {
        public static IQueryable<Character> GetAccessableCharacters(this DatabaseContext db, bool userAdmin, bool userModerator, int? userId = null)
        {
            IQueryable<Character> baseCollection = db.Characters;
            if (userId != null)
                baseCollection = baseCollection.Where(c => c.UserId == userId);

            if  (!userAdmin)
            {
                var publicIds = baseCollection.Where(c => c.IsPublic).Select(c => c.Id);

                if (userModerator)
                {
                    var moderationIds = baseCollection.Where(c => c.Issues.Where(i => !(i.IsAccepted && i.IsResolved)).Any()).Select(c => c.Id);
                    var resultIds = publicIds.Union(moderationIds);
                    return baseCollection.Join(resultIds, b => b.Id, r => r, (b, r) => b);
                }

                return baseCollection.Join(publicIds, b => b.Id, p => p, (b, p) => b);
            }

            return baseCollection;
        }

        public static async Task<bool> IsUserInRole(this DatabaseContext db, int userId, string roleName)
        {
            var role = await db.Roles.Where(r => r.Name == roleName).SingleOrDefaultAsync();
            if (role == null)
                return false;

            return await db.UserRoles
                .Where(ur => ur.UserId == userId && ur.RoleId == role.Id)
                .AnyAsync();
        }

        public static async Task DeleteUserMaterials(this DatabaseContext db, int userId)
        {
            var characters = db.Characters.Where(c => c.UserId == userId);
            db.Characters.RemoveRange(characters);

            var issues = db.Issues.Where(i => i.UserId == userId);
            db.Issues.RemoveRange(issues);

            await db.SaveChangesAsync();
        }
    }

}