﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ElderLists.Startup))]
namespace ElderLists
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
