﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElderListsDatabase.Models
{
    public class Class
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(32)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [StringLength(1024)]
        public string Description { get; set; }

        [Required]
        [ForeignKey("Type")]
        public int TypeId { get; set; }
        public virtual ClassType Type { get; set; }

        [ForeignKey("ClassId")]
        public virtual ICollection<Character> Characters { get; set; }
    }

}