﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElderListsDatabase.Models
{
    public class Alignment
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(32)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [ForeignKey("AlignmentId")]
        public virtual ICollection<Character> Characters { get; set; }

        public virtual ICollection<Link> Links { get; set; }
    }
}