﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Models
{
    public class Gender
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Index(IsUnique = true)]
        [StringLength(32)]
        [Display(Name = "Пол")]
        public string Name { get; set; }

        [ForeignKey("GenderId")]
        public virtual ICollection<Character> Characters { get; set; }
    }
}
