﻿using ElderListsDatabase.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Models
{
    public class Issue
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(1024)]
        public string Text { get; set; }

        [Required]
        public bool IsAccepted { get; set; }

        [Required]
        public bool IsResolved { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        public DateTime LastReview { get; set; }

        [Required]
        public bool IsPrivateLocked { get; set; }

        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<Character> Characters { get; set; }
    }
}
