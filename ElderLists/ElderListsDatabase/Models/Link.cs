﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Models
{
    public class Link
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(32)]
        public string ResourceName { get; set; }

        [Required]
        [StringLength(2048)]
        public string Url { get; set; }

        public virtual ICollection<Alignment> Alignments { get; set; }

        public virtual ICollection<Race> Races { get; set; }
    }
}
