﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElderListsDatabase.Models
{
    public class ClassType
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(32)]
        [Index(IsUnique = true)]
        public string Name { get; set; }

        [ForeignKey("TypeId")]
        public virtual ICollection<Class> Classes { get; set; }
    }
}