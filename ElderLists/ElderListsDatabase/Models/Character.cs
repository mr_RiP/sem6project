﻿using ElderListsDatabase.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Models
{
    public class Character
    {
        [Key]
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Required]
        public bool IsPublic { get; set; }

        [Required]
        [ForeignKey("User")]
        public int UserId { get; set; }
        public virtual User User { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        public DateTime LastChanged { get; set; }

        [Required]
        public DateTime CreationDate { get; set; }

        [Required]
        [ForeignKey("Race")]
        public int RaceId { get; set; }
        public virtual Race Race { get; set; }

        [Required]
        [ForeignKey("Gender")]
        public int GenderId { get; set; }
        public virtual Gender Gender { get; set; }

        [Range(1, int.MaxValue)]
        public int? AbsoluteAge { get; set; }

        [Range(1, int.MaxValue)]
        public int? RelativeAge { get; set; }

        [Required]
        [StringLength(1024)]
        public string Appearance { get; set; }

        [StringLength(512)]
        public string CombatGear { get; set; }

        [StringLength(512)]
        public string CommonGear { get; set; }

        [StringLength(512)]
        public string SpecialSigns { get; set; }

        [Required]
        [ForeignKey("Alignment")]
        public int AlignmentId { get; set; }
        public virtual Alignment Alignment { get; set; }

        [Required]
        [StringLength(1024)]
        public string Personality { get; set; }

        [StringLength(512)]
        public string Vices { get; set; }

        [StringLength(512)]
        public string Virtues { get; set; }

        [Required]
        [ForeignKey("Class")]
        public int ClassId { get; set; }
        public virtual Class Class { get; set; }

        [Required]
        [StringLength(1024)]
        public string Occupation { get; set; }

        [StringLength(512)]
        public string CombatTactics { get; set; }

        [StringLength(512)]
        public string SkillsSource { get; set; }

        public virtual ICollection<Issue> Issues { get; set; }
    }

}
