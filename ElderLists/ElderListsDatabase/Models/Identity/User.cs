﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace ElderListsDatabase.Models.Identity
{
    public class User : IdentityUser<int, UserLogin, UserRole, UserClaim>
    {
        #region Стандартные функции Asp.Net Identity
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User, int> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
        #endregion

        [ForeignKey("UserId")]
        public virtual ICollection<Character> Characters { get; set; }

        [ForeignKey("UserId")]
        public virtual ICollection<Issue> Issues { get; set; }

        [Required]
        public bool IsEmailHidden { get; set; } = true;

        [StringLength(32)]
        public string Telegram { get; set; }
        
        [StringLength(32)]
        public string Discord { get; set; }

        [StringLength(32)]
        public string Skype { get; set; }

        [StringLength(32)]
        public string EsoUserId { get; set; }

        [StringLength(1024)]
        public string About { get; set; }

        [Required]
        public DateTime RegisterDate { get; set; }

        [Required]
        public DateTime LastAction { get; set; }

        [Required]
        public bool IsPersistent { get; set; }
    }


    public class UserStore : UserStore<User, Role, int, UserLogin, UserRole, UserClaim>
    {
        public UserStore(DatabaseContext context) : base(context)
        {
        }
    }
}
