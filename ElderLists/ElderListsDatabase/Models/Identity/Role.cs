﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace ElderListsDatabase.Models.Identity
{
    
    public class Role : IdentityRole<int, UserRole>
    {
        public Role() { }
        public Role(string name) { Name = name; }

        [Required]
        [StringLength(32)]
        public string ViewName { get; set; }
    }

    public class RoleStore : RoleStore<Role, int, UserRole>
    {
        public RoleStore(DatabaseContext context) : base(context)
        {
        }
    }
}
