﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ElderListsDatabase.Models
{
    public class Race
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(32)]
        [Index(IsUnique = true)]
        public string CommonName { get; set; }

        [StringLength(32)]
        public string SpecialName { get; set; }

        [Required]
        public bool IsLongLiver { get; set; }

        [ForeignKey("RaceId")]
        public virtual ICollection<Character> Characters { get; set; }

        public virtual ICollection<Link> Links { get; set; }
    }
}