﻿using ElderListsDatabase.Models;
using ElderListsDatabase.Models.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase
{
    public class DatabaseContext : IdentityDbContext<User,Role,int,UserLogin,UserRole,UserClaim>
    {
        // Identity
        // public virtual DbSet<User> Users { get; set; }   // Перегрузка IdentityDbContext
        // public virtual DbSet<Role> Roles { get; set; }   // Перегрузка IdentityDbContext
        public virtual DbSet<UserClaim> UserClaims { get; set; }
        public virtual DbSet<UserLogin> UserLogins { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }

        public virtual DbSet<Alignment> Alignments { get; set; }
        public virtual DbSet<Character> Characters { get; set; }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<ClassType> ClassTypes { get; set; }
        public virtual DbSet<Race> Races { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<Issue> Issues { get; set; }
        public virtual DbSet<Link> Links { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Создание правильных имен для таблиц Identity
            modelBuilder.Entity<User>().ToTable("Users");
            modelBuilder.Entity<Role>().ToTable("Roles");
            modelBuilder.Entity<UserRole>().ToTable("UserRoles");
            modelBuilder.Entity<UserLogin>().ToTable("UserLogins");
            modelBuilder.Entity<UserClaim>().ToTable("UserClaims");

            // Убираем каскадное удаление
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            #region Таблицы связей для Issues
            modelBuilder.Entity<Character>()
                .HasMany<Issue>(c => c.Issues)
                .WithMany(i => i.Characters)
                .Map(ci =>
                {
                    ci.MapLeftKey("CharacterId");
                    ci.MapRightKey("IssueId");
                    ci.ToTable("CharacterIssues");
                });
            #endregion

            #region Таблицы связей для Links
            modelBuilder.Entity<Race>()
                .HasMany<Link>(r => r.Links)
                .WithMany(l => l.Races)
                .Map(rl =>
                {
                    rl.MapLeftKey("RaceId");
                    rl.MapRightKey("LinkId");
                    rl.ToTable("RaceLinks");
                });

            modelBuilder.Entity<Alignment>()
                .HasMany<Link>(a => a.Links)
                .WithMany(l => l.Alignments)
                .Map(rl =>
                {
                    rl.MapLeftKey("AlignmentId");
                    rl.MapRightKey("LinkId");
                    rl.ToTable("AlignmentLinks");
                });
            #endregion
        }

        public DatabaseContext() : base("DefaultConnection")
        {
        }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }
    }
}
