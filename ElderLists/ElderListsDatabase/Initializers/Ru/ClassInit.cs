﻿using ElderListsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Initializers.Ru
{
    class ClassInit : IInitializer
    {
        public void Seed(DatabaseContext context)
        {
            var typeIds = SeedClassTypes(context);
            SeedCombatClasses(context, typeIds[ClassTypes.Combat]);
            SeedMagicClasses(context, typeIds[ClassTypes.Magic]);
            SeedStealthClasses(context, typeIds[ClassTypes.Stealth]);
        }

        private Dictionary<ClassTypes,int> SeedClassTypes(DatabaseContext context)
        {
            var combat = new ClassType { Name = "Бой" };
            var magic = new ClassType { Name = "Магия" };
            var stealth = new ClassType { Name = "Скрытность" };

            context.ClassTypes.AddOrUpdate(o => o.Name, combat, magic, stealth);
            context.SaveChanges();

            return new Dictionary<ClassTypes, int>
            {
                { ClassTypes.Combat, combat.Id },
                { ClassTypes.Magic, magic.Id },
                { ClassTypes.Stealth, stealth.Id }
            };
        }

        private enum ClassTypes
        {
            Combat,
            Magic,
            Stealth
        }

        private void SeedCombatClasses(DatabaseContext context, int typeId)
        {
            var list = new List<Class>
            {
                new Class
                {
                    Name = "Варвар",
                    Description = "Варвары — гордые, яростные воины, цвет кочевников равнин, племён горцев и морских бродяг."
                },
                new Class
                {
                    Name = "Воин",
                    Description = "Воины — это профессиональные военнослужащие, солдаты, наёмники и искатели приключений, тренированные во владении разнообразными видами оружия и ношении доспехов."
                },
                new Class
                {
                    Name = "Жулик",
                    Description = "Жулики — это смелые искатели приключений и интриганы, которые определённо обладают талантом попадать в неприятности и выбираться из них."
                },
                new Class
                {
                    Name = "Лучник",
                    Description = "Лучники — это бойцы, специализирующиеся на стрелковом оружии и быстром передвижении."
                },
                new Class
                {
                    Name = "Паладин",
                    Description = "Любой воин в тяжёлом доспехе, способный читать заклинания и служащий добрым делам, может называть себя Паладином."
                },
                new Class
                {
                    Name = "Разведчик",
                    Description = "Разведчики полагаются на скрытность, покоряя неизведанные тропы и побеждая врагов, используют стрелковое вооружение и тактику засад в тех случаях, когда схватки не избежать."
                },
                new Class
                {
                    Name = "Рыцарь",
                    Description = "Высокорожденные, или же пробившие себе путь к славе в битвах и турнирах, рыцари — воины цивилизации, обученные манерам и изящному слогу, подчиняющиеся кодексу чести."
                }
            };

            SeedClasses(context, list, typeId);
        }

        private void SeedMagicClasses(DatabaseContext context, int typeId)
        {
            var list = new List<Class>
            {
                new Class
                {
                    Name = "Боевой Маг",
                    Description = "Боевые Маги — колдуны и воины одновременно, тренированные как в использовании смертельно опасных заклинаний, так и в искусстве боя в доспехах и с оружием."
                },
                new Class
                {
                    Name = "Воин Слова",
                    Description = "Воины Слова — это специалисты по чтению заклинаний, которые обучены для поддержки войск в стычках и битвах."
                },
                new Class
                {
                    Name = "Охотник на ведьм",
                    Description = "Охотники на ведьм находят предназначение в выслеживании и уничтожении извращённых проявлений тёмных культов и нечестивого чародейства."
                },
                new Class
                {
                    Name = "Маг",
                    Description = "Большинство магов предпочитают изучать магию ради чистого познания, но они также получают выгоду от её практического применения."
                },
                new Class
                {
                    Name = "Ночной Клинок",
                    Description = "Ночные Клинки — это сведущие в волшебстве люди, использующие свою магическую силу, чтобы улучшать свою подвижность, незаметность и умение ближнего боя."
                },
                new Class
                {
                    Name = "Целитель",
                    Description = "Целители — это знатоки заклинаний, принесшие святую клятву исцелять страждущих и лечить поражённых недугами."
                },
                new Class
                {
                    Name = "Чародей",
                    Description = "Чародеи — признанные мастера волшебства, хоть они и полагаются в основном на зачарованные вещи и призывание существ."
                }
            };

            SeedClasses(context, list, typeId);
        }

        private void SeedStealthClasses(DatabaseContext context, int typeId)
        {
            var list = new List<Class>
            {
                new Class
                {
                    Name = "Акробат",
                    Description = "Акробат — это вежливый эвфемизм для ловких грабителей и форточников."
                },
                new Class
                {
                    Name = "Ассасин",
                    Description = "Ассасины — это убийцы, которые в своём деле полагаются на скрытность и подвижность, чтобы подобраться поближе к жертве незамеченными."
                },
                new Class
                {
                    Name = "Бард",
                    Description = "Барды — это знатоки легенд и рассказчики историй."
                },
                new Class
                {
                    Name = "Вор",
                    Description = "Воры — это медвежатники и карманники. В отличие от разбойников, которые убивают ради добычи, воры обычно предпочитают насилию разного рода трюки и способность передвигаться скрытно."
                },
                new Class
                {
                    Name = "Монах",
                    Description = "Монахи — это те, кто изучает древнее искусство боя без оружия и секретные способы самообороны."
                },
                new Class
                {
                    Name = "Пилигрим",
                    Description = "Пилигримы — это путешественники, искатели истины и просветления."
                },
                new Class
                {
                    Name = "Шпион",
                    Description = "Шпионы — специалисты по обману и ускользанию от ответственности, но они прекрасно обучены самообороне и использованию оружия."
                }
            };

            SeedClasses(context, list, typeId);
        }

        private void SeedClasses(DatabaseContext context, List<Class> list, int typeId)
        {
            foreach (var item in list)
            {
                item.TypeId = typeId;
                context.Classes.AddOrUpdate(o => o.Name, item);
            }
            context.SaveChanges();
        }
    }
}
