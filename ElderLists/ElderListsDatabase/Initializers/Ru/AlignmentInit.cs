﻿using ElderListsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Initializers.Ru
{
    class AlignmentInit : IInitializer
    {
        public void Seed(DatabaseContext context)
        {
            var alignments = SeedAlignments(context);
            var links = SeedLinks(context);

            for (int i = 0; i < alignments.Count; ++i)
                alignments[i].Links = new List<Link> { links[i] };

            context.SaveChanges();
        }

        private List<Alignment> SeedAlignments(DatabaseContext context)
        {
            var list = new List<Alignment>
            {
                new Alignment { Name="Законопослушный добрый" },
                new Alignment { Name="Законопослушный нейтральный" },
                new Alignment { Name="Законопослушный злой" },
                new Alignment { Name="Нейтральный добрый" },
                new Alignment { Name="Истинно нейтральный" },
                new Alignment { Name="Нейтральный злой" },
                new Alignment { Name="Хаотичный добрый" },
                new Alignment { Name="Хаотичный нейтральный" },
                new Alignment { Name="Хаотичный злой" }
            };

            list.ForEach(o => context.Alignments.AddOrUpdate(i => i.Name, o));
            context.SaveChanges();

            return list;
        }

        private List<Link> SeedLinks(DatabaseContext context)
        {
            var list = new List<Link>
            {
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.97.D0.B0.D0.BA.D0.BE.D0.BD.D0.BE.D0.BF.D0.BE.D1.81.D0.BB.D1.83.D1.88.D0.BD.D1.8B.D0.B9_.D0.B4.D0.BE.D0.B1.D1.80.D1.8B.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.97.D0.B0.D0.BA.D0.BE.D0.BD.D0.BE.D0.BF.D0.BE.D1.81.D0.BB.D1.83.D1.88.D0.BD.D1.8B.D0.B9_.D0.BD.D0.B5.D0.B9.D1.82.D1.80.D0.B0.D0.BB.D1.8C.D0.BD.D1.8B.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.97.D0.B0.D0.BA.D0.BE.D0.BD.D0.BE.D0.BF.D0.BE.D1.81.D0.BB.D1.83.D1.88.D0.BD.D1.8B.D0.B9_.D0.B7.D0.BB.D0.BE.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.9D.D0.B5.D0.B9.D1.82.D1.80.D0.B0.D0.BB.D1.8C.D0.BD.D1.8B.D0.B9_.D0.B4.D0.BE.D0.B1.D1.80.D1.8B.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.98.D1.81.D1.82.D0.B8.D0.BD.D0.BD.D0.BE_.D0.BD.D0.B5.D0.B9.D1.82.D1.80.D0.B0.D0.BB.D1.8C.D0.BD.D1.8B.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.9D.D0.B5.D0.B9.D1.82.D1.80.D0.B0.D0.BB.D1.8C.D0.BD.D1.8B.D0.B9_.D0.B7.D0.BB.D0.BE.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.A5.D0.B0.D0.BE.D1.82.D0.B8.D1.87.D0.BD.D1.8B.D0.B9_.D0.B4.D0.BE.D0.B1.D1.80.D1.8B.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.A5.D0.B0.D0.BE.D1.82.D0.B8.D1.87.D0.BD.D1.8B.D0.B9_.D0.BD.D0.B5.D0.B9.D1.82.D1.80.D0.B0.D0.BB.D1.8C.D0.BD.D1.8B.D0.B9" },
                new Link { ResourceName = "Wikipedia (ru)", Url = "https://ru.wikipedia.org/wiki/%D0%9C%D0%B8%D1%80%D0%BE%D0%B2%D0%BE%D0%B7%D0%B7%D1%80%D0%B5%D0%BD%D0%B8%D0%B5_%D0%B2_Dungeons_%26_Dragons#.D0.A5.D0.B0.D0.BE.D1.82.D0.B8.D1.87.D0.BD.D1.8B.D0.B9_.D0.B7.D0.BB.D0.BE.D0.B9" },
            };

            list.ForEach(o => context.Links.AddOrUpdate(i => i.Url, o));
            context.SaveChanges();

            return list;
        }
    }
}
