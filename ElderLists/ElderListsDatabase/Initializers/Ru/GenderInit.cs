﻿using ElderListsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Initializers.Ru
{
    class GenderInit : IInitializer
    {
        public void Seed(DatabaseContext context)
        {
            var list = new List<Gender>
            {
                new Gender { Name = "Мужской" },
                new Gender { Name = "Женский" },
                new Gender { Name = "Иной (выглядит как мужчина)" },
                new Gender { Name = "Иной (выглядит как женщина)" },
            };

            list.ForEach(o => context.Genders.AddOrUpdate(i => i.Name, o));
            context.SaveChanges();
        }
    }
}
