﻿using ElderListsDatabase.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElderListsDatabase.Initializers.Ru
{
    class RaceInit : IInitializer
    {
        public void Seed(DatabaseContext context)
        {
            var races = SeedRaces(context);
            var uespLinks = SeedUespLinks(context);
            var wikiaEnLinks = SeedWikiaEnLinks(context);
            var wikiaRuLinks = SeedWikiaRuLinks(context);

            for (int i = 0; i < races.Count; ++i)
            {
                races[i].Links = new List<Link>
                {
                    uespLinks[i],
                    wikiaEnLinks[i],
                    wikiaRuLinks[i],
                };
            }
            context.SaveChanges();
        }

        private List<Race> SeedRaces(DatabaseContext context)
        {
            var list = new List<Race>
            {
                new Race { CommonName = "Высокий эльф", SpecialName = "Альтмер", IsLongLiver = true },
                new Race { CommonName = "Лесной эльф",  SpecialName = "Босмер",  IsLongLiver = true },
                new Race { CommonName = "Каджит" },
                new Race { CommonName = "Норд" },
                new Race { CommonName = "Тёмный эльф",  SpecialName = "Данмер",  IsLongLiver = true },
                new Race { CommonName = "Аргонианин" },
                new Race { CommonName = "Бретон" },
                new Race { CommonName = "Орк",          SpecialName = "Орсимер" },
                new Race { CommonName = "Редгард" },
                new Race { CommonName = "Имперец" },
            };

            list.ForEach(o => context.Races.AddOrUpdate(i => i.CommonName, o));
            context.SaveChanges();

            return list;
        }

        private List<Link> SeedUespLinks(DatabaseContext context)
        {
            var list = new List<Link>
            {
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Altmer" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Bosmer" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Khajiit" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Nord" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Dunmer" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Argonian" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Breton" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Orc" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Redguard" },
                new Link { ResourceName = "UESP", Url = "http://en.uesp.net/wiki/Lore:Imperial" },
            };

            list.ForEach(o => context.Links.AddOrUpdate(i => i.Url, o));
            context.SaveChanges();

            return list;
        }

        private List<Link> SeedWikiaEnLinks(DatabaseContext context)
        {
            var list = new List<Link>
            {
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Altmer" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Bosmer" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Khajiit" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Nord" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Dunmer" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Argonian" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Breton" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Orsimer" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Redguard" },
                new Link { ResourceName = "Wikia (en)", Url = "http://elderscrolls.wikia.com/wiki/Imperial" },
            };

            list.ForEach(o => context.Links.AddOrUpdate(i => i.Url, o));
            context.SaveChanges();

            return list;
        }

        private List<Link> SeedWikiaRuLinks(DatabaseContext context)
        {
            var list = new List<Link>
            {
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%90%D0%BB%D1%8C%D1%82%D0%BC%D0%B5%D1%80" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%91%D0%BE%D1%81%D0%BC%D0%B5%D1%80" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%9A%D0%B0%D0%B4%D0%B6%D0%B8%D1%82" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%9D%D0%BE%D1%80%D0%B4" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%94%D0%B0%D0%BD%D0%BC%D0%B5%D1%80" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%90%D1%80%D0%B3%D0%BE%D0%BD%D0%B8%D0%B0%D0%BD%D0%B8%D0%BD" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%91%D1%80%D0%B5%D1%82%D0%BE%D0%BD" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%9E%D1%80%D0%BA" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%A0%D0%B5%D0%B4%D0%B3%D0%B0%D1%80%D0%B4" },
                new Link { ResourceName = "Wikia (ru)", Url = "http://ru.elderscrolls.wikia.com/wiki/%D0%98%D0%BC%D0%BF%D0%B5%D1%80%D0%B5%D1%86" },
            };

            list.ForEach(o => context.Links.AddOrUpdate(i => i.Url, o));
            context.SaveChanges();

            return list;
        }
    }
}
