namespace ElderListsDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ElderListsDatabase100 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Alignments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Characters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsPublic = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 128),
                        LastChanged = c.DateTime(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        RaceId = c.Int(nullable: false),
                        GenderId = c.Int(nullable: false),
                        AbsoluteAge = c.Int(),
                        RelativeAge = c.Int(),
                        Appearance = c.String(nullable: false, maxLength: 1024),
                        CombatGear = c.String(maxLength: 512),
                        CommonGear = c.String(maxLength: 512),
                        SpecialSigns = c.String(maxLength: 512),
                        AlignmentId = c.Int(nullable: false),
                        Personality = c.String(nullable: false, maxLength: 1024),
                        Vices = c.String(maxLength: 512),
                        Virtues = c.String(maxLength: 512),
                        ClassId = c.Int(nullable: false),
                        Occupation = c.String(nullable: false, maxLength: 1024),
                        CombatTactics = c.String(maxLength: 512),
                        SkillsSource = c.String(maxLength: 512),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Alignments", t => t.AlignmentId)
                .ForeignKey("dbo.Classes", t => t.ClassId)
                .ForeignKey("dbo.Genders", t => t.GenderId)
                .ForeignKey("dbo.Races", t => t.RaceId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RaceId)
                .Index(t => t.GenderId)
                .Index(t => t.AlignmentId)
                .Index(t => t.ClassId);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 32),
                        Description = c.String(maxLength: 1024),
                        TypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClassTypes", t => t.TypeId)
                .Index(t => t.Name, unique: true)
                .Index(t => t.TypeId);
            
            CreateTable(
                "dbo.ClassTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Genders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 32),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Issues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false, maxLength: 1024),
                        IsAccepted = c.Boolean(nullable: false),
                        IsResolved = c.Boolean(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        LastReview = c.DateTime(nullable: false),
                        IsPrivateLocked = c.Boolean(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsEmailHidden = c.Boolean(nullable: false),
                        Telegram = c.String(maxLength: 32),
                        Discord = c.String(maxLength: 32),
                        Skype = c.String(maxLength: 32),
                        EsoUserId = c.String(maxLength: 32),
                        About = c.String(maxLength: 1024),
                        RegisterDate = c.DateTime(nullable: false),
                        LastAction = c.DateTime(nullable: false),
                        IsPersistent = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.UserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Roles", t => t.RoleId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Races",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CommonName = c.String(nullable: false, maxLength: 32),
                        SpecialName = c.String(maxLength: 32),
                        IsLongLiver = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CommonName, unique: true);
            
            CreateTable(
                "dbo.Links",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ResourceName = c.String(nullable: false, maxLength: 32),
                        Url = c.String(nullable: false, maxLength: 2048),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ViewName = c.String(nullable: false, maxLength: 32),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.CharacterIssues",
                c => new
                    {
                        CharacterId = c.Int(nullable: false),
                        IssueId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CharacterId, t.IssueId })
                .ForeignKey("dbo.Characters", t => t.CharacterId, cascadeDelete: true)
                .ForeignKey("dbo.Issues", t => t.IssueId, cascadeDelete: true)
                .Index(t => t.CharacterId)
                .Index(t => t.IssueId);
            
            CreateTable(
                "dbo.RaceLinks",
                c => new
                    {
                        RaceId = c.Int(nullable: false),
                        LinkId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RaceId, t.LinkId })
                .ForeignKey("dbo.Races", t => t.RaceId, cascadeDelete: true)
                .ForeignKey("dbo.Links", t => t.LinkId, cascadeDelete: true)
                .Index(t => t.RaceId)
                .Index(t => t.LinkId);
            
            CreateTable(
                "dbo.AlignmentLinks",
                c => new
                    {
                        AlignmentId = c.Int(nullable: false),
                        LinkId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AlignmentId, t.LinkId })
                .ForeignKey("dbo.Alignments", t => t.AlignmentId, cascadeDelete: true)
                .ForeignKey("dbo.Links", t => t.LinkId, cascadeDelete: true)
                .Index(t => t.AlignmentId)
                .Index(t => t.LinkId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.AlignmentLinks", "LinkId", "dbo.Links");
            DropForeignKey("dbo.AlignmentLinks", "AlignmentId", "dbo.Alignments");
            DropForeignKey("dbo.Characters", "UserId", "dbo.Users");
            DropForeignKey("dbo.Characters", "RaceId", "dbo.Races");
            DropForeignKey("dbo.RaceLinks", "LinkId", "dbo.Links");
            DropForeignKey("dbo.RaceLinks", "RaceId", "dbo.Races");
            DropForeignKey("dbo.CharacterIssues", "IssueId", "dbo.Issues");
            DropForeignKey("dbo.CharacterIssues", "CharacterId", "dbo.Characters");
            DropForeignKey("dbo.Issues", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserLogins", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserClaims", "UserId", "dbo.Users");
            DropForeignKey("dbo.Characters", "GenderId", "dbo.Genders");
            DropForeignKey("dbo.Characters", "ClassId", "dbo.Classes");
            DropForeignKey("dbo.Classes", "TypeId", "dbo.ClassTypes");
            DropForeignKey("dbo.Characters", "AlignmentId", "dbo.Alignments");
            DropIndex("dbo.AlignmentLinks", new[] { "LinkId" });
            DropIndex("dbo.AlignmentLinks", new[] { "AlignmentId" });
            DropIndex("dbo.RaceLinks", new[] { "LinkId" });
            DropIndex("dbo.RaceLinks", new[] { "RaceId" });
            DropIndex("dbo.CharacterIssues", new[] { "IssueId" });
            DropIndex("dbo.CharacterIssues", new[] { "CharacterId" });
            DropIndex("dbo.Roles", "RoleNameIndex");
            DropIndex("dbo.Races", new[] { "CommonName" });
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.UserLogins", new[] { "UserId" });
            DropIndex("dbo.UserClaims", new[] { "UserId" });
            DropIndex("dbo.Users", "UserNameIndex");
            DropIndex("dbo.Issues", new[] { "UserId" });
            DropIndex("dbo.Genders", new[] { "Name" });
            DropIndex("dbo.ClassTypes", new[] { "Name" });
            DropIndex("dbo.Classes", new[] { "TypeId" });
            DropIndex("dbo.Classes", new[] { "Name" });
            DropIndex("dbo.Characters", new[] { "ClassId" });
            DropIndex("dbo.Characters", new[] { "AlignmentId" });
            DropIndex("dbo.Characters", new[] { "GenderId" });
            DropIndex("dbo.Characters", new[] { "RaceId" });
            DropIndex("dbo.Characters", new[] { "UserId" });
            DropIndex("dbo.Alignments", new[] { "Name" });
            DropTable("dbo.AlignmentLinks");
            DropTable("dbo.RaceLinks");
            DropTable("dbo.CharacterIssues");
            DropTable("dbo.Roles");
            DropTable("dbo.Links");
            DropTable("dbo.Races");
            DropTable("dbo.UserRoles");
            DropTable("dbo.UserLogins");
            DropTable("dbo.UserClaims");
            DropTable("dbo.Users");
            DropTable("dbo.Issues");
            DropTable("dbo.Genders");
            DropTable("dbo.ClassTypes");
            DropTable("dbo.Classes");
            DropTable("dbo.Characters");
            DropTable("dbo.Alignments");
        }
    }
}
