namespace ElderListsDatabase.Migrations
{
    using ElderListsDatabase.Initializers;
    using ElderListsDatabase.Models.Identity;
    using Microsoft.AspNet.Identity;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ElderListsDatabase.DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        private List<IInitializer> GetInitializersList()
        {
            return new List<IInitializer>
            {
                new Initializers.Ru.AlignmentInit(),
                new Initializers.Ru.ClassInit(),
                new Initializers.Ru.RaceInit(),
                new Initializers.Ru.RoleInit(),
                new Initializers.Ru.GenderInit(),
            };
        }

        protected override void Seed(ElderListsDatabase.DatabaseContext context)
        {
            var list = GetInitializersList();
            foreach (var init in list)
                init.Seed(context);
            context.SaveChanges();
        }
    }
}
